﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public float TimeLeft = 185f;
    public Text TimerText;

    // Update is called once per frame
    void Update()
    {
        //Subtracts time past from TimeLeft
        TimeLeft -= Time.deltaTime;
        //sets timer text
        TimerText.text = "Time Remaining: " + Mathf.Floor(TimeLeft);
        if(TimeLeft < 0)
        {
            GameOver();
        }

        //Allows user to reset scene whenever they hit space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }

    void GameOver()
    {
        //reloads scene on timer running out
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
