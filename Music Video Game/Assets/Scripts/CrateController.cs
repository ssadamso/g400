﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateController : MonoBehaviour
{
    public string Direction = "";
    public float Speed;
    public string OtherColor = "";

    private GameObject CurrentConveyor;
    private bool OnConveyor;

    private void Start()
    {
        Direction = "Down";
    }

    private void Update()
    {
        //Calls rigidbody of crate
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();

        //sets velocity of crate based on direction last pressed only if crate is on conveyor
        if (OnConveyor == true)
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                Direction = "Down";
                rb.velocity = new Vector2(0, (Speed * -1));
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                Direction = "Up";
                rb.velocity = new Vector2(0, (Speed));
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Direction = "Left";
                rb.velocity = new Vector2((Speed * -1), 0);

            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                Direction = "Right";
                rb.velocity = new Vector2(Speed, 0);
            }
        }
    }

    //gets tag of object collided with that isn't a boarder or a conveyor
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Conveyor" && collision.gameObject.tag != "Boarder")
        {
            OtherColor = collision.gameObject.tag;
        }
    }

    //sets OtherColor to null once the crates are no longer touching
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Conveyor" && collision.gameObject.tag != "Boarder")
        {
            OtherColor = "";
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Conveyor")
        {
            //sets bool to true so crate can move
            OnConveyor = true;
            CurrentConveyor = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Conveyor" && CurrentConveyor == collision.gameObject)
        {
            Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();

            //sets bool to false when crate leaves conveyor so it no longer moves
            OnConveyor = false;
            rb.velocity = new Vector2(0, 0);
        }
    }
}
